# Magic Hat
(c) 2019 Agis Wichert
[http://www.rohling.de](http://www.rohling.de)

Project Video
[The Magic Hat on YT](https://youtu.be/eMiA-SBCUTA)



### MQTT Channels
## Color
"hat/color/in" - ESP gets it's color (HEX format) on this channel
"hat/color/out" - ESP publishes from time to time the selected color

## Modes
"hat/mode/in" - ESP gets it's mode (0-6) on this channel
"hat/mode/out" - ESP publishes from time to time the selected mode


### Modes

## Off (0)
All LEDs are turned off

## On (1) 
All LEDs are on

## Random (2) *
Colors changes randomly

## Kight Rider (3)
K.I.T.T mode 

## Blue Light (4)
Like a blue light of a fire truck

## Driver (5) *
White on the front, red on the back

## Blicky (6)
Makes the LEDs blink

* Color cannot be selected in this mode
