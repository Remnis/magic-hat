/*
 * MQTT-Magic Hat
 * (c) 2018 Agis Wichert
 * 
 * Youtube:
 * https://www.youtube.com/user/nenioc187
 * 
 * Web
 * http://www.rohling.de
 */
// ESP 8266
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// ESPName
char* EspName = "MQTT-Magic-Hat";

// WiFi & MQTT Server
const char* ssid = "***";
const char* password = "****";
const char* mqtt_server = "192.168.1.99";

// MQTT Subscribtions
const char* ModeSubscribe = "hat/mode/in"; // ESP recieves here mode info
const char* ModePublish = "hat/mode/out";

const char* ColorSubscribe = "hat/color/in"; // ESP recieves here timer info
const char* ColorPublish = "hat/color/out";

long lastPublish = millis();
long publishInterval = 1700; // 1,7 sec

WiFiClient espClient;
PubSubClient pubClient(espClient);

// Neopixels
#include <Adafruit_NeoPixel.h>
#define NeoPIN D4
#define NUM_LEDS 19
int brightness = 150;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, NeoPIN, NEO_RGB + NEO_KHZ800);
String lastColor = "#000000";
int colorR = 0;
int colorG = 0;
int colorB = 0;
int pixelPos = 0;


// animations
long animationTime = 110; // 120 ms
long animationPos = millis();
int hatMode = 0;
int animationDirection = 1; // -1

bool stateChanged = false;

void setup() {
  Serial.begin(115200);
  
  // Neopixel
  strip.setBrightness(brightness);
  strip.begin();
  strip.show();
  
  delay(50);
  clearPixels();
  strip.show();

  // WiFi
  setup_wifi();
  
  // MQTT Server
  Serial.print("Connecting to MQTT...");
  // connecting to the mqtt server
  pubClient.setServer(mqtt_server, 1883);
  pubClient.setCallback(callback);
  Serial.println(F("done!"));
  
  // reset last publish timer
  lastPublish = millis();

}

void loop() {
  if (!pubClient.connected()) {
    delay(100);
    reconnect();
  }
  pubClient.loop();
   // check mode
  checkMode();
  // publish topics
  publishTopics();

}

void checkMode(){
  if(hatMode == 0){
    // off
    if((animationPos + animationTime) < millis()){
      if(stateChanged){
        clearPixels();
        strip.show();
        stateChanged = false;
      }
      animationPos = millis();
    }
  }
  else if(hatMode == 1){
    // all on
    if(stateChanged){
      setNeoColor(lastColor);
      stateChanged = false;
    }
  }
  else if(hatMode == 2){
    // random colors
    modeRandom();
  }
  else if(hatMode == 3){
    // K.I.T.T. Mode
    modeKightRider();
  }
  else if(hatMode == 4){
    // blue light
    modeBlueLight();
  }
  else if(hatMode == 5){
    // driver
    if(stateChanged){
      driverMode();
      stateChanged = false;
    }
  }
  else if(hatMode == 6){
    // blinky
    modeBlinky();
  }
  
}

// #############
// #Region Modes

// sets 3 LEDs to a random color (2)
void modeRandom(){
  if((animationPos + animationTime + 100) < millis()){
    int led = 0;
    int rRed = random(0,254);
    int rBlue = random(0,250);
    int rGreen = random(0,240);
    for(int i = 0; i < 3; i++){
      led = random(0,NUM_LEDS);
      strip.setPixelColor(led, strip.Color( rGreen, rRed, rBlue ) );
    }
    strip.show();
    animationPos = millis();
  }
}


// K.I.T.T. (3)
void modeKightRider(){
  if((animationPos + animationTime) < millis()){
    loadColor(lastColor);
    // clear display
    clearPixels();
    
    // set color
    for(int i = 0; i < 3; i++){
      int diff = i * 5;
      int cB = colorB - diff;
      int cR = colorR - diff;
      int cG = colorG - diff;
      if(cB < 0){ 
        cB = 0;
      }
      else if(cB > 255){
        cB = 255;
      }
      if(cR < 0){ 
        cR = 0;
      }
      else if(cR > 255){
        cR = 255;
      }
      if(cG < 0){ 
        cG = 0;
      }
      else if(cG > 255){
        cG = 255;
      }
      
      strip.setPixelColor(pixelPos + i, strip.Color( cG, cR, cB ));
      strip.setPixelColor(pixelPos - i, strip.Color( cG, cR, cB ));
    }
    strip.show();
    if(pixelPos == NUM_LEDS){
      animationDirection = -1;
    }
    else if(pixelPos == 0){
      animationDirection = 1;
    }
    pixelPos = pixelPos + animationDirection;
    //
    animationPos = millis();
  }
}

// 3 LEDS rotating light (4)
void modeBlueLight(){
  if((animationPos + animationTime) < millis()){
     loadColor(lastColor);
     // clear display
     clearPixels();
     if(pixelPos > NUM_LEDS){
        pixelPos = 0;
     }
     // pixel before and after
     int pre = pixelPos - 1;
     int post = pixelPos +1;
     if(pre < 0){
        pre = NUM_LEDS;
     }
     if(post > NUM_LEDS){
      post = 0;
     }
     strip.setPixelColor(pre, strip.Color( colorG, colorR, colorB ) );
     strip.setPixelColor(pixelPos, strip.Color( colorG, colorR, colorB ) );
     strip.setPixelColor(post, strip.Color( colorG, colorR, colorB ) );
     strip.show();
     pixelPos++;
     animationPos = millis();
  }
}

// front white, back red (5)
void driverMode(){
  int midPos = NUM_LEDS /2;
  clearPixels();
  for(int i = midPos -2; i <= midPos + 2; i++){
   strip.setPixelColor(i, strip.Color( 254, 254, 254) ); 
  }
  
   strip.setPixelColor(0, strip.Color( 5, 254, 5) );
   strip.setPixelColor(1, strip.Color( 5, 254, 5) );
   strip.setPixelColor(NUM_LEDS-1, strip.Color( 5, 254, 5) );
   strip.setPixelColor(NUM_LEDS-2, strip.Color( 5, 254, 5) );
   strip.show();
}

// make every 2nd blink (6) (
void modeBlinky(){
  if((animationPos + animationTime) < millis()){
    if(pixelPos == 0){
      // every odd led is on
      loadColor(lastColor);
    }
    else{
      // every odd led is off
      loadColor("#000000");
    }
    for(int i = 0; i < NUM_LEDS; i = i+2){
      strip.setPixelColor(i, strip.Color( colorG, colorR, colorB ) ); 
    }
    if(pixelPos == 0){
      loadColor("#000000");
      pixelPos = 1;
    }
    else{
      loadColor(lastColor);
      pixelPos = 0;
    }
    for(int i = 1; i < NUM_LEDS; i = i+2){
      strip.setPixelColor(i, strip.Color( colorG, colorR, colorB ) ); 
    } 
    strip.show();
    animationPos = millis();
    
  }
}

// #Endregion Modes
// ################

// MQTT server connect
void reconnect() {
  // try to reconnect
  if (!pubClient.connected()) {
    // Attempt to connect
    if (pubClient.connect(EspName)) {
      Serial.println(F("connected"));

      // subsrcibe to mode
      pubClient.subscribe(ModeSubscribe);
      pubClient.publish(ModePublish, "0");
      pubClient.loop();

      // subsrcibe to color
      pubClient.subscribe(ColorSubscribe);
      pubClient.publish(ColorPublish, "#000000");
      pubClient.loop();

      Serial.println(F("Done"));
      
    } else {
      Serial.print(F("failed, rc="));
      Serial.print(pubClient.state());
      Serial.println(F(" try again in 5 seconds"));
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


// Network connection
void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println(F("WiFi connected"));
  Serial.println(F("IP address: "));
  Serial.println(WiFi.localIP());
}

void clearPixels(){
  for(int i=0; i < NUM_LEDS; i++) {
      strip.setPixelColor(i, strip.Color( 0, 0, 0 ) );
  }
  Serial.println("Clearing all pixels (need to call show)");
}

// setting all Neopixels to one color
void setNeoColor(String value){
    // load color value
    loadColor(value);
    for(int i=0; i < NUM_LEDS; i++) {
      strip.setPixelColor(i, strip.Color( colorG, colorR, colorB ) );
    }
    strip.show();
}

// converting string color to rgb color
void loadColor(String value){
    int number = (int) strtol( &value[1], NULL, 16);
    colorR = number >> 16;
    colorG = number >> 8 & 0xFF;
    colorB = number & 0xFF;
}

// handles the recieved MQTT messages
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print(F("Message arrived ["));
  Serial.print(topic);
  Serial.print("] !");

  if(String(topic).equals(String(ColorSubscribe))){
    // changing color
    String color("#");
    for (int i = 0; i < length; i++) {
      Serial.print((char)payload[i]);
      if(i > 0){
        color = color + String((char)payload[i]);  
      }
    }
    if(!lastColor.equals(color)){
      Serial.println(F("New Color"));
      //stateChanged = true;
    }
    
    stateChanged = true;
    lastPublish = 0;
    lastColor = color;
    
  }
  else if(String(topic).equals(String(ModeSubscribe))){
    // changing mode
    String m("");
    for (int i = 0; i < length; i++) {
        m = m + String((char)payload[i]);
    }
    hatMode = m.toInt();
    stateChanged = true;
    Serial.print(m);
  }
 
  else{
    Serial.println(F("No fitting topic found"));
  }
  Serial.println();
}

// publish topics to sync with all devices
void publishTopics(){
  
  if(( lastPublish + publishInterval) < millis()){
    // mode
    char b[2];
    String str;
    str=String(hatMode);
    str.toCharArray(b,2);
    pubClient.publish( ModePublish, b );  

    // current color
    char msg[7];
    lastColor.toCharArray(msg, lastColor.length()+1);
    pubClient.publish(ColorPublish, msg);

    
    // reset pub timer
    lastPublish = millis();
  }
}
